//
//  DrinkRow.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 29/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import SwiftUI

struct DrinkRow: View {
    
    var categoryName: String
    var festivals: [Festival]
    
    var body: some View {
        
        VStack(alignment: .leading){
            Text(self.categoryName)
                .font(.title)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(self.festivals) { festival in
                        
                        NavigationLink(destination: DrinkDetail(festival: festival)) {
                            DrinkItem(festival: festival)
                                .frame(width: 300)
                                .padding(.trailing, 30)
                        }
                    }
                }
            }
        }
    }
}

struct DrinkRow_Previews: PreviewProvider {
    static var previews: some View {
        DrinkRow(categoryName: "Sagre belle", festivals: festivalData)
    }
}
