//
//  FirebaseReference.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 23/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum FColletionReference: String {
    case User
    case Menu
    case Order
    case Basket
}

func FirebaseReference (_ collectionReference: FColletionReference) -> CollectionReference {

    return Firestore.firestore().collection(collectionReference.rawValue)
}
