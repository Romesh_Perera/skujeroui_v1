//
//  Extensions.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 03/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
