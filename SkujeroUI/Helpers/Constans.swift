//
//  Constans.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 24/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation

public let userDefaults = UserDefaults.standard

// MARK: Festivals

public let kID = "id"
public let kNAME = "name"
public let kIMAGENAME = "imageName"
public let kPrice = "price"
public let kDESCRIPTION = "description"
public let kCATEGORY = "category"

// MARK: Orders
public let KFOODIDS = "foodIds"
public let kOWNERID = "owenerID"
public let kCUSTOMERID = "costumerID"
public let kAMOUNT = "amount"


// MARK: FUser

public let kEMAIL = "email"
public let kFIRSTNAME = "firstname"
public let kLASTNAME = "lastname"
public let kFULLNAME = "fullname"
public let kCURRENTUSER = "currentUser"
public let kFULLADDRESS = "fullAddress"
public let kPHONENUMBER = "phoneNumber"
public let KONBOARD = "onBoard"
