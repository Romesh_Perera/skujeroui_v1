//
//  DrinkItem.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 31/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import SwiftUI

struct DrinkItem: View {
    
    var festival: Festival
    
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 16){
            Image(festival.imageName)
                .renderingMode(.original)
                .aspectRatio(contentMode: .fill)
                .frame(width: 300, height: 170)
                .cornerRadius(10)
                .shadow(radius: 10)
            
            VStack(alignment: .leading, spacing: 5) {
                
                Text(festival.name)
                    .foregroundColor(.primary)
                    .font(.headline)
                
                Text(festival.description)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .frame(height: 40)
            }
        }
     
    }
}

struct DrinkItem_Previews: PreviewProvider {
    static var previews: some View {
        
        DrinkItem(festival: festivalData[1])
    }
}
