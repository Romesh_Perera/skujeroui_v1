//
//  OrderBasketView.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 02/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import SwiftUI

struct OrderBasketView: View {
    
    @ObservedObject var basketListener = BasketListener()
    
    
    var body: some View {
        
        NavigationView {
            
            List {
                Section {
                    ForEach(self.basketListener.oderBasket?.items ?? []) {
                        festival in
                        HStack {
                            Text(festival.name)
                            Spacer()
                            Text("$\(festival.price.clean)")
                        }
                    }
                    .onDelete { (indexSet) in
                        self.deleteItems(at: indexSet)
                    }
                    
                    Section {
                        NavigationLink(destination: HomeView()) {
                            Text("Place Order")
                        }
                    }.disabled(self.basketListener.oderBasket?.items.isEmpty ?? true)
                }
            } // end of List
            .navigationBarTitle("Ordine")
            .listStyle(GroupedListStyle())
            
        } // end of navigationview
    }
    
    func deleteItems(at offsets: IndexSet) {
        self.basketListener.oderBasket.items.remove(at: offsets.first!)
        self.basketListener.oderBasket.saveBasketToFirestore()
    }
}

struct OrderBasketView_Previews: PreviewProvider {
    static var previews: some View {
        OrderBasketView()
    }
}
