//
//  Festival.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 22/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation
import SwiftUI


struct Festival: Identifiable, Hashable {
    
    var id: String
    var name: String
    var imageName: String
    var category: Category
    var description: String
    var price: Double
    
}

func festivalDictionaryFrom(festival: Festival) -> [String: Any] {
    
    return NSDictionary(objects: [ festival.id,
                                   festival.name,
                                   festival.category.rawValue,
                                   festival.imageName,
                                   festival.description,
                                   festival.price
                                 ],
                        forKeys: [kID as NSCopying,
                                  kNAME as NSCopying,
                                  kIMAGENAME as NSCopying,
                                  kCATEGORY as NSCopying,
                                  kDESCRIPTION as NSCopying,
                                  kPrice as NSCopying
    ]) as! [String : Any]
}

func createMenu() {
    for festival in festivalData {
        FirebaseReference(.Menu).addDocument(data: festivalDictionaryFrom(festival: festival))
    }
}


enum Category: String, CaseIterable, Codable, Hashable {
    case nearMe
    case popular
    case feature
}


//MARK: Dummy data

let festivalData = [
    //NEARME
    Festival(id: UUID().uuidString, name: "Espresso", imageName: "espresso", category: Category.nearMe, description: "Espresso is the purest distillation of the coffee bean. It doesn’t refer to a bean or blend, but actually the preparation method.", price: 2.50),
    
    Festival(id: UUID().uuidString, name: "Americano", imageName: "americano", category: Category.nearMe, description: "An Americano Coffee is an Espresso-based coffee drink with no special additions. Actually it is a shot of Espresso with hot water poured in it. A well-prepared Americano has the subtle aroma and flavour like Espresso. Benefits of Americano Coffee it has a lighter body and less bitterness.", price: 2.00),
    
    Festival(id: UUID().uuidString, name: "Cappuccino", imageName: "cappuccino", category: Category.nearMe, description: "Outside of Italy, cappuccino is a coffee drink that today is typically composed of double espresso and hot milk, with the surface topped with foamed milk. Cappuccinos are most often prepared with an espresso machine.", price: 2.50),
    
    Festival(id: UUID().uuidString, name: "Latte", imageName: "latte", category: Category.nearMe, description: "A typical latte is made with six to eight ounces of steamed milk and one shot of espresso. Larger lattes are often made with a double shot of espresso.", price: 2.50),
        
                    
    //POPULAR
    Festival(id: UUID().uuidString, name: "Filter Classic", imageName: "filter coffee", category: Category.popular, description: "Filter coffee brewing involves pouring hot water over coffee grounds. Gravity then pulls the water through the grounds, facilitating extraction, and dispenses it into a mug or carafe placed below.", price: 2.00),
    
    Festival(id: UUID().uuidString, name: "Filter Decaf", imageName: "decaf", category: Category.popular, description: "Filter coffee brewing involves pouring hot water over coffee grounds. Gravity then pulls the water through the grounds, facilitating extraction, and dispenses it into a mug or carafe placed below.", price: 2.00),

    Festival(id: UUID().uuidString, name: "Cold Brew", imageName: "cold brew", category: Category.popular, description: "Cold brew is really as simple as mixing ground coffee with cool water and steeping the mixture in the fridge overnight.", price: 2.50),

    Festival(id: UUID().uuidString, name: "Cold Brew Latte", imageName: "brew latte", category: Category.popular, description: "To make a weaker brew, add 2 parts cold brew coffee to 1 part hot water. For a stronger brew, use a 4:1 ratio. Cold Brew Concentrate for Iced Lattes: use 3 ounces coffee beans per 2.5 cups of water.", price: 2.00),

    
    
    //FEATURE
    Festival(id: UUID().uuidString, name: "Frappe", imageName: "frappe", category: Category.feature, description: "Frappé coffee is a Greek iced coffee drink made from instant coffee, water and sugar. Accidentally invented by a Nescafe representative in 1957 in Thessaloniki", price: 5.00),
    
    Festival(id: UUID().uuidString, name: "Freddo Espresso", imageName: "freddo espresso", category: Category.feature, description: "A Freddo Espresso is basically 1 shot of espresso poured hot into a metal canister. It's then mixed with an electric blender, using a couple of ice cubes, and sugar is also added during the mixing process.", price: 5.00),
    
    Festival(id: UUID().uuidString, name: "Freddo Cappucciono", imageName: "freddo cappuccino", category: Category.feature, description: "The Freddo Cappuccino is pretty much a Freddo Espresso with a lovely creamy foam layered on top, so if you prefer milk in your coffee, this one is for you!", price: 4.00),
    
    Festival(id: UUID().uuidString, name: "Americano", imageName: "ice americano", category: Category.feature, description: "An Americano Coffee is an Espresso-based coffee drink with no special additions. Actually it is a shot of Espresso with hot water poured in it. A well-prepared Americano has the subtle aroma and flavour like Espresso. Benefits of Americano Coffee it has a lighter body and less bitterness.", price: 2.00),
    
    Festival(id: UUID().uuidString, name: "Iced Latte", imageName: "iced latte", category: Category.feature, description: "The latte is one of the most iconic espresso drinks, favored for its frothy foam topping. In this refreshing iced version, you can easily create foam with cold milk—no steamer needed.", price: 2.50)
    
]

