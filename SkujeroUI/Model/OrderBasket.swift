//
//  OrderBasket.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 01/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation
import Firebase

class OrderBasket: Identifiable {
    
    var id: String!
    var ownerId: String!
    var items: [Festival] = []
    
    var total: Double {
        if items.count > 0 {
            return items.reduce(0) {$0 + $1.price}
        } else {
            return 0
        }
    }
    
    func add(_ item: Festival) {
        items.append(item)
    }
    
    func remove(_ item: Festival) {
        if let index = items.firstIndex(of: item) {
            items.remove(at: index)
        }
    }
    
    func emptyBasket() {
        self.items = []
        saveBasketToFirestore()
    }
    
    func saveBasketToFirestore() {
        FirebaseReference(.Basket).document(self.id).setData(basketDictionaryFrom(self))
    }
    
}

func basketDictionaryFrom(_ basket: OrderBasket) -> [String: Any] {
    var allFestivalIds: [String] = []
    
    for festival in basket.items {
        allFestivalIds.append(festival.id)
    }
    return NSDictionary(
            objects: [
                basket.id,
                basket.ownerId,
                allFestivalIds],
        
            forKeys: [
                kID as NSCopying,
                kOWNERID as NSCopying,
                KFOODIDS as NSCopying]) as! [String: Any]
}
