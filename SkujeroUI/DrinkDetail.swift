//
//  DrinkDetail.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 01/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import SwiftUI

struct DrinkDetail: View {
    
    @State private var showingAlert = false
    
    var festival: Festival
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
            
            ZStack(alignment: .bottom) {
                Image(festival.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                
                Rectangle()
                    .frame(height: 80)
                    .foregroundColor(.black)
                    .opacity(0.35)
                    .blur(radius: 10)
                
                HStack {
                    VStack(alignment: .leading, spacing: 8) {
                        Text(festival.name)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    }
                    .padding(.leading)
                    .padding(.bottom)
                    
                    Spacer()
                } // end of Hstack
            }// end of Zstack
                .listRowInsets(EdgeInsets())
            
            Text(festival.description)
                .foregroundColor(.primary)
                .font(.body)
                .lineLimit(5)
                .padding()
            
            HStack {
                Spacer()
                OrderButton(showAlert: $showingAlert, festival: festival)
                Spacer()
            }
            .padding(.top, 50)
            
        } //end of ScrollView
            .edgesIgnoringSafeArea(.top)
            .navigationBarHidden(true)
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("👍 Aggiunto al carello"), message: Text("Potrai sempre modificare il tuo carello nella pagina dedicata"), dismissButton: .default(Text("OK")))
        }
        
        
    }
}

struct DrinkDetail_Previews: PreviewProvider {
    static var previews: some View {
        DrinkDetail(festival: festivalData.first!)
    }
}

struct OrderButton : View {
    
    @ObservedObject var basketListener = BasketListener()
    @Binding var showAlert: Bool
    
    var festival : Festival
    
    var body : some View {
        Button(action: {
            self.showAlert.toggle()
            self.addItemToBasket()
        }) {
            Text("Aggiungi al carrello")
        }
        .frame(width: 200, height: 50)
        .foregroundColor(.white)
        .font(.headline)
        .background(Color.orange)
        .cornerRadius(10)
    }
    
    private func addItemToBasket() {
        
        var orderBasket: OrderBasket!
    
        // check if user has the basket
        
        if self.basketListener.oderBasket != nil {
            orderBasket = self.basketListener.oderBasket
        } else {
            orderBasket = OrderBasket()
            orderBasket.ownerId = "123"
            orderBasket.id = UUID().uuidString
        }
        orderBasket.add(self.festival)
        orderBasket.saveBasketToFirestore()
        
    }
    
    
    
}
