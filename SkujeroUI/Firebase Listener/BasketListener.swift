//
//  BasketListener.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 02/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation
import Firebase

class BasketListener: ObservableObject {
    
    @Published var oderBasket : OrderBasket!
    
    init() {
        dowloadBasket()
    }
    
    
    func dowloadBasket() {
        FirebaseReference(.Basket).whereField(kOWNERID, isEqualTo: "123").addSnapshotListener { (snapshot, error) in
            
            guard let snapshot = snapshot else { return }
            
            if !snapshot.isEmpty {
                let basketData = snapshot.documents.first!.data()
                
                getFestivalFromFirestore(withIds: basketData[KFOODIDS] as? [String] ?? [] ) { (allFestival) in
                    let basket = OrderBasket()
                    basket.ownerId = basketData[kOWNERID] as? String
                    basket.id = basketData[kID] as? String
                    basket.items = allFestival
                    self.oderBasket = basket
                }
            }
        }
    }
}

func getFestivalFromFirestore(withIds: [String], completion: @escaping (_ festivalArray: [Festival]) -> Void) {
    
    var count = 0
    var festivalArray: [Festival] = []
    
    if withIds.count == 0 {
        completion(festivalArray)
        return
    }
    
    for festivalId in withIds {
        
        FirebaseReference(.Menu).whereField(kID, isEqualTo: festivalId).getDocuments { (snapshot, error) in
            
            guard let snapshot = snapshot else { return }
            
            if !snapshot.isEmpty {
                
                let festivalData = snapshot.documents.first!
                
                festivalArray.append(Festival(
                    id: festivalData[kID] as? String ?? UUID().uuidString,
                    name: festivalData[kNAME] as? String ?? "non conosciuto",
                    imageName: festivalData[kIMAGENAME] as? String ?? "non conosciuto",
                    category: Category(rawValue: festivalData[kCATEGORY] as? String ?? "nearMe") ?? .nearMe,
                    description: festivalData[kDESCRIPTION] as? String ?? "Non c'è descrizione",
                    price: festivalData[kPrice] as? Double ?? 0.0))
                
                
            } else {
                print("non ci sono festival")
                completion(festivalArray)
            }
            
            if count == withIds.count {
                completion(festivalArray)
            }
        }
        
        
    }
    
    
    
    
    
}


