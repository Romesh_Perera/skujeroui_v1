//
//  FestivalListener.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 01/02/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import Foundation
import Firebase
import SwiftUI

class FestivalListener: ObservableObject {
    
    @Published var festivals: [Festival] = []
    
    init() {
        dowloadFestivals()
    }
    
    
    func dowloadFestivals() {
        
        FirebaseReference(.Menu).getDocuments { (snapshot, error) in
            
            guard let snapshot = snapshot else { return }
            
            if !snapshot.isEmpty {
                self.festivals = FestivalListener.drinkFromDictionary(snapshot)
            }
        }
    }
    
    static func drinkFromDictionary(_ snapshot: QuerySnapshot) -> [Festival] {
        
        var allFestivals: [Festival] = []
        
        for snapshot in snapshot.documents {
            
            let festivalData = snapshot.data()
            
            allFestivals.append(Festival(
                id: festivalData[kID] as? String ?? UUID().uuidString,
                name: festivalData[kNAME] as? String ?? "Non trovato",
                imageName: festivalData[kIMAGENAME] as? String ?? "Non trovato",
                category: Category(rawValue: festivalData[kCATEGORY] as? String ?? "Vicino a me") ?? .nearMe,
                description: festivalData[kDESCRIPTION] as? String ?? "Non c'è nessuna descrizione",
                price: festivalData[kPrice]as? Double ?? 0))
        }
        
        return allFestivals
    }
    
}
