//
//  ContentView.swift
//  SkujeroUI
//
//  Created by Romesh Singhabahu on 21/01/2020.
//  Copyright © 2020 Romesh Singhabahu. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var festivalListener = FestivalListener()
    @State private var showingBasket = false
    
    var categories: [String: [Festival]] {
        .init(
            grouping: festivalListener.festivals,
            by: {$0.category.rawValue})
    }
    
    var body: some View {
        
        NavigationView {
            
            List(categories.keys.sorted(), id: \String.self) { key in
                DrinkRow(categoryName: "\(key)",
                    festivals: self.categories[key]!)
                    .frame(height: 320)
                    .padding(.top)
                    .padding(.bottom)
            }
            
                .navigationBarTitle(Text("Sagre"))
                .navigationBarItems(
                    leading:
                    Button(action: {
                        //code for Firebase to push data
                        createMenu()
                        print("log out")
                    }, label: {
                        Text("Logout")
                    }),
                                    
                    trailing:
                    Button(action: {
                        //code
                        self.showingBasket.toggle()
                    }, label: {
                        Image("basket")
                    })
                        .sheet(isPresented: $showingBasket) {
                            OrderBasketView()
                    }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
